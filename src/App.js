import './App.css';
import PropsCustom from './props/propsCustom';
function App() {
  return (
    <div className="App">
        <PropsCustom />
    </div>
  );
}

export default App;
