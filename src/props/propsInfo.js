import React from "react";

export default function PropsInfo(props) {
  return (
    <>
      <p>Name: {props.name}</p>
      <p>Email: {props.email}</p>
      <p>Sdt: {props.sdt}</p>
      <p>
        {props.data.map((item) => (
          <p>{item.name}</p>
        ))}
      </p>
      <select>
        {props.data.map((item) => (
          <option value={item._id}>{item.name}</option>
        ))}
      </select>
    </>
  );
}
